import csv
import struct

class GameboyDisassembler:
    ''' Disassemble binary files to gameboy bytecode'''

    def __init__(self, fileName):
        self.opcodes = {}
        self.loadOpcodeFile('instruction_set.txt')
        with open(fileName, 'rb') as romFile:
            for byte in iter(lambda: romFile.read(1), ''):
                code = struct.unpack('B', byte)[0]

                # Check if code is valid
                if code not in self.opcodes:
                    print("Undefined opcode: " + hex(code))
                    return

                # code 0xCB is a prefix for 16bit opcode
                if code == int('0xCB', 16):
                    byte2 = romFile.read(1)
                    byte = byte2+byte
                    code = struct.unpack('<H', byte)[0]

                # Check if 16bit opcode valid
                if code not in self.opcodes:
                    print("Undefined opcode: " + hex(code))
                    return
                
                size = int(self.opcodes[code]['length'])
                toRead = size-len(byte);
                addressByte = romFile.read(toRead)
                address = 0
                if (toRead == 1):
                    address = struct.unpack('B', addressByte)[0]
                elif (toRead == 2):
                    address = struct.unpack('<H', addressByte)[0]

                print(self.opcodes[code]['mnemonic'].format(address))

    def loadOpcodeFile(self, fileName):
        with open(fileName, 'r') as instructionFile:
            instructionReader = csv.DictReader(instructionFile, skipinitialspace=True)
            
            for row in instructionReader:
                opcode = int(row['code'], 16)
                self.opcodes[opcode] = row

GameboyDisassembler('DMG_ROM.bin')
