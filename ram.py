class Ram:
    def __init__(self, size):
        self.size = size
        self.ram = bytearray(size)
        self.maxword = pow(2, 8)
        self.maxdword = pow(2, 16)

    def getWord(self, addr):
        return self.ram[addr]

    def setWord(self, addr, value):
        self.ram[addr] = value % self.maxword

    def getDWord(self, addr):
        return self.ram[addr]+self.ram[addr+1]*256

    def setDWord(self, addr, value):
        value = value % self.maxdword
        high, low = divmod(value, 0x100)
        self.ram[addr] = low
        self.ram[addr+1] = high
