from ram import Ram
from rom_loader import RomLoader

class MemoryMap:
    def __init__(self, cartridgeFile):
        self.bootstrap = RomLoader('DMG_ROM.bin')
        self.cartridge = RomLoader(cartridgeFile)
        self.ram = Ram(pow(2, 16))

    def getWord(self, addr):
        bank = self.getBank(addr)
        return bank.getWord(addr)

    def setWord(self, addr, value):
        bank = self.getBank(addr)
        bank.setWord(addr, value)
        #if addr < int('0x9FFF', 16) and addr >= int('0x8000', 16):
        #    print("Writing to " + hex(addr) + ", val " + hex(value)) 
        
    def getDWord(self, addr):
        bank = self.getBank(addr)
        return bank.getDWord(addr)
    
    def setDWord(self, addr, value):
        bank = self.getBank(addr)
        bank.setDWord(addr, value)

    def getBank(self, addr):
        if addr < int('0x0100', 16):
            return self.bootstrap
        elif addr < int('0x8000', 16):
            return self.cartridge
        elif addr < int('0xFFFF', 16):
            return self.ram
