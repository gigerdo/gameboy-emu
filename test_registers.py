from registers import Registers
import unittest

class TestSequenceFunctions(unittest.TestCase):
    def setUp(self):
        self.regs = Registers()

    def test_HL(self):
        val = int('0xABCD', 16) 
        self.regs.setHL(val)
        temp = self.regs.getHL()
        self.assertEqual(val, temp)

        maxval = pow(2, 16) +5
        self.regs.setHL(maxval)
        maxtemp = self.regs.getHL()
        self.assertEqual(maxtemp, 5)
        
    def test_DE(self):
        val = int('0xABCD', 16) 
        self.regs.setDE(val)
        temp = self.regs.getDE()
        self.assertEqual(val, temp)
        maxval = pow(2, 16) +5
        self.regs.setDE(maxval)
        maxtemp = self.regs.getDE()
        self.assertEqual(maxtemp, 5)

    def test_BC(self):
        val = int('0xABCD', 16) 
        self.regs.setBC(val)
        temp = self.regs.getBC()
        self.assertEqual(val, temp)
        maxval = pow(2, 16) +5
        self.regs.setBC(maxval)
        maxtemp = self.regs.getBC()
        self.assertEqual(maxtemp, 5)

    def test_AF(self):
        val = int('0xABCD', 16) 
        self.regs.setAF(val)
        temp = self.regs.getAF()
        self.assertEqual(val, temp)
        maxval = pow(2, 16) +5
        self.regs.setAF(maxval)
        maxtemp = self.regs.getAF()
        self.assertEqual(maxtemp, 5)

if __name__ == '__main__':
    unittest.main()
