from registers import Registers
from cpu import Cpu
from memorymap import MemoryMap
from display import Display
import csv
import traceback

class Emulator:
    ''' Based on
         http://nocash.emubase.de/pandocs.htm#cpuinstructionset
         http://gameboy.mongenel.com/dmg/opcodes.html
         http://gbdev.gg8.se/wiki/articles/Gameboy_Bootstrap_ROM
         http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
    '''
    def __init__(self, cartridgeFile):
        self.opcodes = {}
        self.loadOpcodeFile('instruction_set.txt')
        self.regs = Registers()
        self.memory = MemoryMap(cartridgeFile)
        self.cpu = Cpu(self.memory)
        self.display = Display(self.memory)

    def loadOpcodeFile(self, fileName):
        with open(fileName, 'r') as instructionFile:
            instructionReader = csv.DictReader(instructionFile, skipinitialspace=True)
            for row in instructionReader:
                opcode = int(row['code'], 16)
                self.opcodes[opcode] = row

    def nextInstruction(self):
        code = self.memory.getWord(self.regs.PC)
        self.regs.increasePC(1)
        
        # Check if code is valid
        if code not in self.opcodes:
            print("Undefined opcode: " + hex(code))
            return -1
        # code 0xCB is a prefix for 16bit opcode
        elif code == int('0xCB', 16):
            byte2 = self.memory.getWord(self.regs.PC)
            self.regs.increasePC(1)
            code = byte2+code*256
            # Check if 16bit opcode valid
            if code not in self.opcodes:
                print("Undefined opcode: " + hex(code))
                return -1
            
            instruction = {'opcode':code, 'clocks': self.opcodes[code]['cycles']}
            return instruction
        else:
            instruction = {'opcode':code, 'clocks': self.opcodes[code]['cycles']}
            
            size = int(self.opcodes[code]['length'])
            toRead = size-1;
            
            if toRead > 0:
                address = 0
                if (toRead == 1):
                    address = self.memory.getWord(self.regs.PC)
                    self.regs.increasePC(1)
                elif (toRead == 2):
                    address = self.memory.getDWord(self.regs.PC)
                    self.regs.increasePC(2)
                instruction['data'] = address
            
            return instruction

    def run(self):
        clocks = 0
        while True:
            instruction = self.nextInstruction()
            if instruction == -1:
                break

            '''if self.regs.PC > int('0x0055',16) and self.regs.PC < int('0x006d',16):
                if 'data' in instruction:
                    print(hex(instruction['opcode']) + ' / ' + self.opcodes[instruction['opcode']]['mnemonic'].format(instruction['data']))
                else:
                    print(hex(instruction['opcode']) + ' / ' + self.opcodes[instruction['opcode']]['mnemonic'])
                '''
            try:
                self.cpu.apply(instruction, self.regs)
                tmp = instruction['clocks'].split("/")
                clocks += int(tmp[0])

                clocks = self.display.updateMode(clocks)  
                self.display.drawTilePatternTable()
                #self.display.draw()
            except AttributeError: 
                print("Error, PC: " + hex(self.regs.PC))
                print("Instruction: " + hex(instruction['opcode']))
                print("A:  " + hex(self.regs.A))
                print("C:  " + hex(self.regs.C))
                print("HL: " + hex(self.regs.getHL()))
                print("ZF: " + str(self.regs.zf))
                print("H: " + str(self.regs.h))
                print("N: " + str(self.regs.n))
                print("CY: " + str(self.regs.cy))
                traceback.print_exc()
                break

            if self.regs.PC == int('0xfe', 16):
                print('DMG finished...')
                self.display.drawTilePatternTable()
                break
            

emu = Emulator('test_cartridge.gb')
emu.run()
