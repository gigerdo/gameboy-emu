class Registers:
    def __init__(self):
        self.SP = 0 # stack pointer
        self.PC = 0 # program counter
        self.A = 0
        self.F = 0
        self.B = 0
        self.C = 0
        self.D = 0
        self.E = 0
        self.H = 0
        self.L = 0
        self.zf = False # zero flag
        self.n = False # add/sub flag
        self.h = False # half carry flag
        self.cy = False # carry flag

        self.maxword = pow(2, 8)
        self.maxdword = pow(2, 16)

    def increaseSP(self, inc):
        self.SP += inc
        self.SP = self.SP % self.maxdword

    def setSP(self, sp):
        self.SP = sp % self.maxdword

    def increasePC(self, inc):
        self.PC += inc
        self.PC = self.PC % self.maxdword

    def setPC(self, pc):
        self.PC = pc % self.maxdword

    def getAF(self):
        return self.A*256+self.F

    def setAF(self, data):
        self.A, self.F = divmod(data % self.maxdword, 0x100)
        self.A %= self.maxword
        self.F %= self.maxword

    def getBC(self):
        return self.B*256+self.C

    def setBC(self, data):
        self.B, self.C = divmod(data % self.maxdword, 0x100)
        self.B %= self.maxword
        self.C %= self.maxword

    def getDE(self):
        return self.D*256+self.E

    def setDE(self, data):
        self.D, self.E = divmod(data % self.maxdword, 0x100)
        self.D %= self.maxword
        self.E %= self.maxword

    def setD(self, data):
        self.D = data % self.maxword

    def setE(self, data):
        self.E = data % self.maxword

    def getHL(self):
        return self.H*256+self.L

    def setHL(self, data):
        self.H, self.L = divmod(data % self.maxdword, 0x100)
        self.H %= self.maxword
        self.L %= self.maxword

    def setH(self, data):
        self.H = data % self.maxword

    def setL(self, data):
        self.L = data % self.maxword

    def setA(self, data):
        self.A = data % self.maxword
        
    def setB(self, data):
        self.B = data % self.maxword

    def setC(self, data):
        self.C = data % self.maxword

