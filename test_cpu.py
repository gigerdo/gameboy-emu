from registers import Registers
from memorymap import MemoryMap
from cpu import Cpu
import unittest

class TestSequenceFunctions(unittest.TestCase):
    def setUp(self):
        self.memory = MemoryMap()
        self.regs = Registers()
        self.cpu = Cpu(self.memory)

    def test_0xaf(self):
        self.regs.A = 11
        instruction = {'opcode':int('0xaf', 16)}
        self.cpu.apply(instruction, self.regs)
        self.assertEqual(self.regs.zf, True)

    def test_0xcb7c(self):
        self.regs.H = 1 << 7
        instruction = {'opcode':int('0xcb7c', 16)}
        self.cpu.apply(instruction, self.regs)
        self.assertEqual(self.regs.zf, False)

if __name__ == '__main__':
    unittest.main()
