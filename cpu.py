import sys

class Cpu:
    def __init__(self, memory):
        self.memory = memory

    def apply(self, instruction, regs):
        opcode = instruction['opcode']

        opFunction = '_'+hex(opcode)

        if 'data' in instruction:
            getattr(self, opFunction)(regs, instruction['data'])
        else:
            getattr(self, opFunction)(regs)

    def twos_comp(self, val, bits):
        """compute the 2's compliment of int value val"""
        if( (val&(1<<(bits-1))) != 0 ):
            val = val - (1<<bits)
        return val

    def _0x0(self, regs):
        # NOP
        print("NOP")
    def _0x1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4(self, regs):
        # INC B
        b = regs.B
        b = b+1
        regs.setB(b)
        regs.zf = b==0
        regs.n = False
        regs.h = b > 0
    def _0x5(self, regs):
        # DEC B
        b = regs.B
        regs.setB(b-1)
        regs.zf = regs.B==0
        regs.n = True
        regs.h = b-1 < 0
    def _0x6(self, regs, data):
        # LD B,d8
        regs.setB(data)
    def _0x7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd(self, regs):
        # DEC C
        c = regs.C
        c = c - 1
        regs.setC(c)
        regs.zf = c == 0
        regs.n = True
        regs.h = c < 0
    def _0xe(self, regs, data):
        # LD C,d8
        regs.setC(data)
    def _0xf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x10(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x11(self, regs, data):
        # LD DE,d16
        regs.setDE(data)
    def _0x12(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x13(self, regs):
        # INC DE
        de = regs.getDE()
        regs.setDE(de+1)
    def _0x14(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x15(self, regs):
        # DEC D
        d = regs.D
        d = d - 1
        regs.setD(d)
        regs.zf = d == 0
        regs.n = True
        regs.h = d < 0
    def _0x16(self, regs, data):
        # LD D,d8
        regs.setD(data)
    def _0x17(self, regs):
        # RLA
        a = regs.A
        overflowbit = ((1<<7)&a) > 0
        a = a << 1
        a = a & regs.cy
        regs.cy = overflowbit
        regs.zf = False
        regs.h = False
        regs.n = False
    def _0x18(self, regs, data):
        # JR r8
        sigData = self.twos_comp(data, 8)
        regs.increasePC(sigData)
    def _0x19(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x1a(self, regs):
        # LD A, (DE)
        val = self.memory.getWord(regs.getDE())
        regs.setA(val)
    def _0x1b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x1c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x1d(self, regs):
        # DEC E
        e = regs.E
        e = e - 1
        regs.setE(e)
        regs.zf = e == 0
        regs.n = True
        regs.h = e < 0
    def _0x1e(self, regs, data):
        # LD E,d8
        regs.setE(data)
    def _0x1f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x20(self, regs, data):
        # JR NZ,r8
        if regs.zf == False:
            regs.increasePC(self.twos_comp(data, 8))
    def _0x21(self, regs, data):
        #LD HL,d16
        regs.setHL(data)
    def _0x22(self, regs):
        # LD (HL+),A
        hl = regs.getHL()
        self.memory.setWord(hl, regs.A)
        regs.setHL(hl+1)
    def _0x23(self, regs):
        # INC HL
        regs.setHL(regs.getHL()+1)
    def _0x24(self, regs):
        # INC H
        h = regs.H
        h = h+1
        regs.setH(h)
        regs.zf = h==0
        regs.n = False
        regs.h = h > 0
    def _0x25(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x26(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x27(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x28(self, regs, data):
        # JR Z,r8
        if regs.zf:
            sigData = self.twos_comp(data, 8)
            regs.increasePC(sigData)
    def _0x29(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x2e(self, regs, data):
        # LD L,d8
        regs.setL(data)
    def _0x2f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x30(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x31(self, regs, data):
        regs.setSP(data)
    def _0x32(self, regs):
        # LD (HL-),A"
        addr = regs.getHL()
        self.memory.setWord(addr, regs.A)
        addr -= 1
        regs.setHL(addr)
    def _0x33(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x34(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x35(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x36(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x37(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x38(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x39(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x3a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x3b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x3c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x3d(self, regs):
        # DEC A
        a = regs.A
        a = a - 1
        regs.setA(a)
        regs.zf = a == 0
        regs.n = True
        regs.h = a < 0
    def _0x3e(self, regs, data):
        # LD A,d8
        regs.setA(data)
    def _0x3f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x40(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x41(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x42(self, regs):
        # LD B, D
        d = regs.D
        regs.setB(d)
    def _0x43(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x44(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x45(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x46(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x47(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x48(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x49(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x4f(self, regs):
        # LD C, A
        regs.setC(regs.A)

    def _0x50(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x51(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x52(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x53(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x54(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x55(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x56(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x57(self, regs):
        # LD D, A
        regs.setD(regs.A)
    def _0x58(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x59(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x5f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x60(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x61(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x62(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x63(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x64(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x65(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x66(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x67(self, regs):
        # LD H,A
        regs.setH(regs.A)
    def _0x68(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x69(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x6f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x70(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x71(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x72(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x73(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x74(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x75(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x76(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x77(self, regs):
        # LD (HL),A
        self.memory.setWord(regs.getHL(), regs.A)
    def _0x78(self, regs):
        # LD A,B
        regs.setA(regs.B)
    def _0x79(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x7a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x7b(self, regs):
        # LD A,E
        e = regs.E
        regs.setA(e)
    def _0x7c(self, regs):
        # LD A,H
        regs.setA(regs.H)
    def _0x7d(self, regs):
        # LD A, L
        regs.setA(regs.L)
    def _0x7e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x7f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x80(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x81(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x82(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x83(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x84(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x85(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x86(self, regs):
        # ADD A,(HL)
        a = regs.A
        hl = regs.getHL()
        val = self.memory.getWord(hl)
        res = val+a
        regs.setA(res)
        a = regs.A
        regs.zf = a == 0
        regs.n = 0
        regs.h = a < res
        regs.cy = a < res
    def _0x87(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x88(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x89(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x8f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0x90(self, regs):
        # SUB B
        a = regs.A
        b = regs.B
        res = a-b
        regs.setA(res)
        regs.zf = res == 0
        regs.n = True
        regs.h = res < 0
        regs.cy = res < 0
    def _0x91(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x92(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x93(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x94(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x95(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x96(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x97(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x98(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x99(self, regs):
        # SBC A,C
        a = regs.A
        c = regs.C
        cy = regs.cy
        a = a - c - cy
        regs.setA(a)
        regs.cy = a < 0
        regs.h = a < 0
        regs.zf = a == 0
        regs.n = True
    def _0x9a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0x9f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xa0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xa9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xaa(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xab(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xac(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xad(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xae(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xaf(self, regs):
        regs.A = regs.A ^ regs.A
        regs.zf = regs.A == 0

    def _0xb0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xb9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xba(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xbb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xbc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xbd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xbe(self, regs):
        # CP (HL)
        a = regs.A
        val = self.memory.getWord(regs.getHL())
        res = a - val
        regs.zf = res == 0
        regs.n = True
        regs.h = res < 0
        regs.cy = res < 0
    def _0xbf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xc0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc1(self, regs):
        # POP BC
        sp = regs.SP
        val = self.memory.getDWord(sp)
        regs.setBC(val)
        regs.setSP(sp+2)
    def _0xc2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc3(self, regs, data):
        # JP a16
        regs.increasePC(data)
        print("PC> "+hex(regs.PC))
    def _0xc4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc5(self, regs):
        # PUSH BC
        sp = regs.SP
        self.memory.setDWord(sp-2, regs.getBC())
        regs.setSP(sp-2)
    def _0xc6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xc9(self, regs):
        # RET
        sp = regs.SP
        regs.setPC(self.memory.getDWord(sp))
        regs.setSP(sp+2)
    def _0xca(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcd(self, regs, data):
        # CALL a16
        sp = regs.SP
        self.memory.setDWord(sp-2, regs.PC)
        regs.setPC(data)
        regs.setSP(sp-2)
    def _0xce(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xd0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xd9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xda(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xdb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xdc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xdd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xde(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xdf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xe0(self, regs, data):
        # LDH (a8),A
        self.memory.setWord(int('0xFF00',16)+data, regs.A)
    def _0xe1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe2(self, regs, data):
        # LD (C), A
        self.memory.setWord(int('0xFF00',16)+regs.C, regs.A)
    def _0xe3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xe9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xea(self, regs, data):
        # LD (a16),A
        self.memory.setWord(data, regs.A)
    def _0xeb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xec(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xed(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xee(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xef(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xf0(self, regs, data):
        # LDH A,(a8)
        val = self.memory.getWord(int('0xFF00',16)+data)
        regs.setA(val)
    def _0xf1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xf9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xfa(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xfb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xfc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xfd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xfe(self, regs, data):
        # CP d8
        a = regs.A
        res = a - data
        regs.cy = data > a
        regs.h = data > a
        regs.n = True
        regs.zf = res == 0
    def _0xff(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)


    ## 
    ## CB PREFIX
    ##

    def _0xcb00(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb01(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb02(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb03(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb04(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb05(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb06(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb07(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb08(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb09(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb0f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb10(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb11(self, regs):
        # RL C
        c = regs.C
        overflowbit = ((1<<7)&c) > 0
        c = c << 1
        c = c & regs.cy
        regs.cy = overflowbit
        regs.zf = regs.C == 0
        regs.h = False
        regs.n = False
    def _0xcb12(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb13(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb14(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb15(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb16(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb17(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb18(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb19(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb1f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb20(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb21(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb22(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb23(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb24(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb25(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb26(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb27(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb28(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb29(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb2f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb30(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb31(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb32(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb33(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb34(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb35(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb36(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb37(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb38(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb39(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb3f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb40(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb41(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb42(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb43(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb44(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb45(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb46(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb47(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb48(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb49(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb4f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb50(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb51(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb52(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb53(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb54(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb55(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb56(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb57(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb58(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb59(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb5f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb60(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb61(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb62(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb63(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb64(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb65(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb66(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb67(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb68(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb69(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb6f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb70(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb71(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb72(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb73(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb74(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb75(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb76(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb77(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb78(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb79(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb7a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb7b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb7c(self, regs):
        # BIT 7,H
        mask = 1 << 7
        regs.zf = (regs.H & mask) == 0
        regs.n = 0
        regs.h = 1
    def _0xcb7d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb7e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb7f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb80(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb81(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb82(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb83(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb84(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb85(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb86(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb87(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb88(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb89(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb8f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcb90(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb91(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb92(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb93(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb94(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb95(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb96(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb97(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb98(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb99(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9a(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9b(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9c(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9d(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9e(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcb9f(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcba0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcba9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbaa(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbab(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbac(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbad(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbae(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbaf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcbb0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbb9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbba(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbbb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbbc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbbd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbbe(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbbf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcbc0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbc9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbca(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbcb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbcc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbcd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbce(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbcf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcbd0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbd9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbda(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbdb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbdc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbdd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbde(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbdf(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcbe0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbe9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbea(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbeb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbec(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbed(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbee(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbef(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

    def _0xcbf0(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf1(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf2(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf3(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf4(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf5(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf6(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf7(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf8(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbf9(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbfa(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbfb(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbfc(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbfd(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbfe(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)
    def _0xcbff(self, regs):
        print("undefined opcode: " + sys._getframe().f_code.co_name)

