from tkinter import *

class Display:
    def __init__(self, mem):
        self.memory = mem
        self.memory.setWord(int('0xFF44', 16), 144)
        self.master = Tk()
        self.canvas = Canvas(self.master, width=160, height=144)
        self.canvas.pack()
        self.debugCanvas = Canvas(self.master, width=512, height=512)
        self.debugCanvas.pack()
        self.colors = ('light yellow', 'yellow', 'gold', 'dark goldenrod')

    def updateMode(self, clocks):
        stat = self.getLCDCStatus()
        mode = stat & int('0b00000011',2)
        rest = stat & int('0b11111000',2)

        ly = self.getLY()
        if ly == self.getLYC():
            rest += 4
        
        if mode == 0 and clocks >= 204:
            clocks = 0
            ly += 1
            if ly >= 144:
                mode = 1
            else:
                mode = 2
        elif mode == 2 and clocks >= 80:
            mode =3
            clocks = 0
        elif mode == 3 and clocks >= 169:
            mode = 0
            clocks = 0
        elif mode == 1 and clocks >= 456:
            ly +=1 
            clocks = 0
            if ly > 153:
                ly = 0
                mode = 2

        self.setLCDCStatus(rest + mode)
        #if clocks == 0:
        #    print(str(ly) + ': ' + str(mode))

        self.setLY(ly)
        return clocks

    def draw(self):
        start = int('0x8000', 16)
        #self.memory.getWord(start+x*
        #self.canvas.create_rectangle(5, 5, 1, 1, fill="blue")

    def drawTilePatternTable(self):
        addr = int('0x8000', 16)
        end = int('0x8FFF', 16)
        x = 0
        y = 0
        while addr < end:            
            for line in range(8):
                a = self.memory.getWord(addr+2*line)
                b = self.memory.getWord(addr+2*line+1)
                for pixel in range(8):
                    pa = a >> 7-pixel
                    pb = b >> 7-pixel
                    colorid = pb*2 + pa
                    self.debugCanvas.create_rectangle(x*8+pixel, y*8+line, 1, 1, fill=self.colors[colorid])
            x += 1
            if x >= 64:
                x = 0
                y += 1
            if y >= 64:
                y = 0
            addr += 16

    def getLCDControl(self):
        return self.memory.getWord(int('0xFF40', 16))
        # 0xFF40
        #Bit 7 - LCD Display Enable             (0=Off, 1=On)
        #Bit 6 - Window Tile Map Display Select (0=9800-9BFF, 1=9C00-9FFF)
        #Bit 5 - Window Display Enable          (0=Off, 1=On)
        #Bit 4 - BG & Window Tile Data Select   (0=8800-97FF, 1=8000-8FFF)
        #Bit 3 - BG Tile Map Display Select     (0=9800-9BFF, 1=9C00-9FFF)
        #Bit 2 - OBJ (Sprite) Size              (0=8x8, 1=8x16)
        #Bit 1 - OBJ (Sprite) Display Enable    (0=Off, 1=On)
        #Bit 0 - BG Display (for CGB see below) (0=Off, 1=On)

    def setLCDCStatus(self, val):
        self.memory.setWord(int('0xFF41', 16), val)
        
    def getLCDCStatus(self):
        return self.memory.getWord(int('0xFF41', 16)) 
        # 0xFF41
        # Bit 6 - LYC=LY Coincidence Interrupt (1=Enable) (Read/Write)
        # Bit 5 - Mode 2 OAM Interrupt         (1=Enable) (Read/Write)
        # Bit 4 - Mode 1 V-Blank Interrupt     (1=Enable) (Read/Write)
        # Bit 3 - Mode 0 H-Blank Interrupt     (1=Enable) (Read/Write)
        # Bit 2 - Coincidence Flag  (0:LYC<>LY, 1:LYC=LY) (Read Only)
        # Bit 1-0 - Mode Flag       (Mode 0-3, see below) (Read Only)
        #           0: During H-Blank
        #           1: During V-Blank
        #           2: During Searching OAM-RAM
        #           3: During Transfering Data to LCD Driver

        '''
        Mode 0: The LCD controller is in the H-Blank period and
          the CPU can access both the display RAM (8000h-9FFFh)
          and OAM (FE00h-FE9Fh)

          Mode 1: The LCD contoller is in the V-Blank period (or the
                  display is disabled) and the CPU can access both the
                  display RAM (8000h-9FFFh) and OAM (FE00h-FE9Fh)

          Mode 2: The LCD controller is reading from OAM memory.
                  The CPU <cannot> access OAM memory (FE00h-FE9Fh)
                  during this period.

          Mode 3: The LCD controller is reading from both OAM and VRAM,
                  The CPU <cannot> access OAM and VRAM during this period.
                  CGB Mode: Cannot access Palette Data (FF69,FF6B) either.
          '''

    def getScrollY(self):
        # 0xFF42
        return self.memory.getWord(int('0xFF42', 16))

    def getScrollX(self):
        # 0xFF43
        return self.memory.getWord(int('0xFF43', 16))

    def getLY(self):
        # 0xFF44
        return self.memory.getWord(int('0xFF44', 16))

    def setLY(self, val):
        # 0xFF44
        self.memory.setWord(int('0xFF44', 16), val)

    def getLYC(self):
        # 0xFF45
        return self.memory.getWord(int('0xFF45', 16))

    def getWindowY(self):
        # 0xFF4A
        return self.memory.getWord(int('0xFF4A', 16))
    def getWindowX(self):
        # 0xFF4B
        return self.memory.getWord(int('0xFF4B', 16))-7

    def getBGPalette(self):
        # 0xFF47
        return self.memory.getWord(int('0xFF47', 16))

    def getObject0Palette(self):
        # 0xFF48
        return self.memory.getWord(int('0xFF48', 16))

    def getObject1Palette(self):
        # 0xFF49
        return self.memory.getWord(int('0xFF49', 16))

    def getVRAMBank(self):
        # 0xFF47
        return self.memory.getWord(int('0xFF4F', 16)) & 1
