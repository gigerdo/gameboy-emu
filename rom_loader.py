import csv
import struct
import array

class RomLoader:
    ''' Disassemble binary files to gameboy bytecode'''

    def __init__(self, fileName):
        self.rom = bytearray()
        
        with open(fileName, 'rb') as romFile:
            self.rom = romFile.read()

    def getWord(self, address):
        if address < len(self.rom):
            return self.rom[address]
        else:
            print("bad access @" + hex(address))
            return 0

    def getDWord(self, address):
        return self.rom[address]+self.rom[address+1]*256

    '''
    def setDWord(self, address, val):
        print("ROM write @" + hex(address) + " = " + hex(val))

    def setWord(self, address, val):
        print("ROM write @" + hex(address) + " = " + hex(val))
    '''
